     1.4            M                                       ANTEX VERSION / SYST
A                                                           PCV TYPE / REFANT
########################################################### COMMENT
General hint for satellite antenna corrections:             COMMENT
   All values in this file refer to an IGS-specific axis    COMMENT
   convention which differs from manufacturer               COMMENT
   specifications for certain satellite types. The IGS      COMMENT
   convention allows for a uniform description of the       COMMENT
   spacecraft attitude for all satellites applying a yaw-   COMMENT
   steering attitude control. Detailed definitions are      COMMENT
   provided in Montenbruck et al. (2015).                   COMMENT
                                                            COMMENT
GPS satellite antenna corrections:                          COMMENT
  - z-offsets:                                              COMMENT
    + satellite-specific                                    COMMENT
    + based on reprocessed (1994-2014) and operational      COMMENT
      (2015-2016) AC SINEX files                            COMMENT
    + weighted mean of seven ACs (CODE, ESA, GFZ, JPL, MIT, COMMENT
      NRCan, ULR)                                           COMMENT
    + solutions aligned to ITRF2014                         COMMENT
    + trend-corrected to epoch 2010.0                       COMMENT
    + analyzed and combined by IGN and TUM                  COMMENT
    + L1 and L2 set to the results for the ionosphere-free  COMMENT
      linear combination                                    COMMENT
    + block-specific mean values for historical satellites  COMMENT
      (active prior to 1994)                                COMMENT
  - phase center variations:                                COMMENT
    + block-specific                                        COMMENT
    + purely nadir-dependent (no azimuth-dependence)        COMMENT
    + maximum nadir angle: 14 degrees (Block I), 17 degrees COMMENT
      (Block II/IIA/IIR-A/IIR-B/IIR-M/IIF)                  COMMENT
    + adopted from igs05.atx                                COMMENT
    + solutions aligned to IGb00                            COMMENT
    + unweighted mean of two ACs (GFZ, TUM)                 COMMENT
    + L1 and L2 set to the results for the ionosphere-free  COMMENT
      linear combination                                    COMMENT
    + Block IIF: adopted from igs08.atx, solutions aligned  COMMENT
      to IGS08, unweighted mean of CODE and ESOC            COMMENT
    + extension for nadir angles beyond 14 degrees based on COMMENT
      LEO data from 2009 analyzed by CODE                   COMMENT
  - x- and y-offsets:                                       COMMENT
    + block-specific (except for Block IIR)                 COMMENT
    + based on manufacturer information                     COMMENT
    + satellite-specific corrections from pre-flight        COMMENT
      calibrations for Block IIR (Dilssner et al., 2016:    COMMENT
      Evaluating the pre-flight GPS Block IIR/IIR-M antenna COMMENT
      phase pattern measurements, IGS Workshop 2016)        COMMENT
                                                            COMMENT
GLONASS satellite antenna corrections:                      COMMENT
  - z-offsets:                                              COMMENT
    + satellite-specific                                    COMMENT
    + based on reprocessed and operational CODE (2002-2016) COMMENT
      and ESOC (2009-2016) solutions                        COMMENT
    + solutions aligned to ITRF2014                         COMMENT
    + L1 and L2 set to the results for the ionosphere-free  COMMENT
      linear combination                                    COMMENT
    + block-specific mean values for historical satellites  COMMENT
      (active between 1998 and 2002)                        COMMENT
  - phase center variations:                                COMMENT
    + block-specific (common parameters for GLONASS and     COMMENT
      GLONASS-M), satellite-specific values for R714        COMMENT
    + purely nadir-dependent (no azimuth-dependence)        COMMENT
    + maximum nadir angle: 15 degrees                       COMMENT
    + adopted from igs08.atx                                COMMENT
    + solutions aligned to ITRF2008                         COMMENT
    + unweighted mean of two ACs (CODE, ESOC)               COMMENT
    + L1 and L2 set to the results for the ionosphere-free  COMMENT
      linear combination                                    COMMENT
  - x- and y-offsets:                                       COMMENT
    + block-specific                                        COMMENT
    + based on manufacturer information                     COMMENT
                                                            COMMENT
Galileo satellite antenna corrections:                      COMMENT
  - x-, y- and z-offsets:                                   COMMENT
    + GALILEO-0A/B: satellite- and frequency-specific       COMMENT
      values based on ESA information (Zandbergen and       COMMENT
      Navarro, 2008: Specification of Galileo and GIOVE     COMMENT
      space segment properties relevant for satellite laser COMMENT
      ranging, ESA-EUING-TN/10206)                          COMMENT
    + GALILEO-1/2: block-specific; rounded mean of two ACs  COMMENT
      (DLR, GFZ; Steigenberger et al., 2016: Estimation of  COMMENT
      satellite antenna phase center offsets for Galileo.   COMMENT
      J Geod, doi: 10.1007/s00190-016-0909-6); E1, E5a,     COMMENT
      E5b, E5, and E6 set to the results for the            COMMENT
      ionosphere-free linear combination of E1 and E5a      COMMENT
  - phase center variations:                                COMMENT
    + wild card values                                      COMMENT
    + maximum nadir angle: 13 degrees (extended to 16       COMMENT
      degrees for E201/E202 taking into account the         COMMENT
      increased orbital eccentricity)                       COMMENT
                                                            COMMENT
BeiDou satellite antenna corrections:                       COMMENT
  - x-, y- and z-offsets:                                   COMMENT
    + block-specific                                        COMMENT
    + conventional MGEX values                              COMMENT
    + duplicate entries for C01 and C02 to support the use  COMMENT
      of RINEX 3.01 and 3.02 observation codes of B1        COMMENT
  - phase center variations:                                COMMENT
    + wild card values                                      COMMENT
    + maximum nadir angle: 14 degrees (BEIDOU-2M),          COMMENT
      9 degrees (BEIDOU-2G/I)                               COMMENT
                                                            COMMENT
QZSS satellite antenna corrections:                         COMMENT
  - x-, y- and z-offsets:                                   COMMENT
    + satellite-specific                                    COMMENT
    + frequency-specific                                    COMMENT
    + based on JAXA information (provided by S. Kogure)     COMMENT
    + values refer to the main L-ANT antenna (consideration COMMENT
      of the SAIF antenna requires an extension of the      COMMENT
      ANTEX format for signal-specific values)              COMMENT
  - phase center variations:                                COMMENT
    + wild card values                                      COMMENT
    + maximum nadir angle: 10 degrees                       COMMENT
                                                            COMMENT
IRNSS satellite antenna corrections:                        COMMENT
  - x-, y- and z-offsets:                                   COMMENT
    + block-specific                                        COMMENT
    + based on ISRO information (provided by A.S. Ganeshan) COMMENT
  - phase center variations:                                COMMENT
    + wild card values                                      COMMENT
    + maximum nadir angle: 14 degrees (taking into account  COMMENT
      the off-nadir pointing navigation antenna)            COMMENT
                                                            COMMENT
Receiver antenna corrections:                               COMMENT
  - absolute elevation- and azimuth-dependent corrections   COMMENT
    from robot calibrations in the field performed by       COMMENT
    Geo++ GmbH for GPS and, to some extent, for GLONASS     COMMENT
    (http://www.geopp.de/gnpcvdb)                           COMMENT
  - purely elevation-dependent corrections from relative    COMMENT
    field calibrations performed by NGS and/or other        COMMENT
    institutions; converted to absolute corrections by      COMMENT
    adding                                                  COMMENT
      d_offset (AOAD/M_T_abs - AOAD/M_T_rel)  and           COMMENT
      d_pattern (AOAD/M_T_abs - AOAD/M_T_rel)               COMMENT
    (http://www.ngs.noaa.gov/ANTCAL,                        COMMENT
     ftp://igs.org/pub/station/general/igs_01.pcv)          COMMENT
  - IMPORTANT hints:                                        COMMENT
    If no corrections are available for a combination of an COMMENT
    antenna with one specific radome, the values for the    COMMENT
    corresponding antenna without a radome (radome code:    COMMENT
    NONE) are used within the IGS.                          COMMENT
    If no corrections for the GLONASS frequencies are       COMMENT
    available, the values for the GPS frequencies are used  COMMENT
    within the IGS instead.                                 COMMENT
                                                            COMMENT
References:                                                 COMMENT
  Schmid R, Dach R, Collilieux X, Jaeggi A, Schmitz M,      COMMENT
    Dilssner F (2016) Absolute IGS antenna phase center     COMMENT
    model igs08.atx: status and potential improvements.     COMMENT
    J Geod 90(4): 343-364, doi: 10.1007/s00190-015-0876-3   COMMENT
  Rothacher M, Schmid R (2010) ANTEX: The Antenna Exchange  COMMENT
    Format, Version 1.4 (ftp://igs.org/pub/station/general/ COMMENT
    antex14.txt)                                            COMMENT
  Montenbruck O, Schmid R, Mercier F, Steigenberger P, Noll COMMENT
    C, Fatkulin R, Kogure S, Ganeshan AS (2015) GNSS        COMMENT
    satellite geometry and attitude models. Adv Space Res   COMMENT
    56(6): 1015-1029, doi: 10.1016/j.asr.2015.06.019        COMMENT
                                                            COMMENT
                                                            COMMENT
Major changes w.r.t. ngs08.atx:                             COMMENT
  - satellite antenna corrections consistent with ITRF2014/ COMMENT
    IGS14                                                   COMMENT
  - satellite antenna z-offsets based on the results of     COMMENT
    more ACs (GPS: 7 instead of 5, GLONASS: still 2)        COMMENT
  - satellite antenna z-offsets trend-corrected to epoch    COMMENT
    2010.0                                                  COMMENT
  - satellite-specific x- and y-offsets from pre-flight     COMMENT
    calibrations for the Block IIR satellites               COMMENT
  - type-specific robot-based receiver antenna corrections  COMMENT
    updated with results from recent individual antenna     COMMENT
    calibrations                                            COMMENT
  - conversion of relative receiver antenna corrections     COMMENT
    with updated AOAD/M_T values                            COMMENT
  - additional ROBOT calibrations:                          COMMENT
      AERAT2775_43    SPKE                                  COMMENT
      AOAD/M_T        DUTD                                  COMMENT
      ASH700936D_M    SCIS                                  COMMENT
      ASH700936E      SCIS                                  COMMENT
      ASH701073.1     NONE                                  COMMENT
      ASH701073.1     SCIS                                  COMMENT
      ASH701073.1     SNOW                                  COMMENT
      ASH701945C_M    SCIS                                  COMMENT
      ASH701945D_M    NONE                                  COMMENT
      ASH701945D_M    SCIS                                  COMMENT
      ASH701945E_M    SCIS                                  COMMENT
      ASH701945G_M    NONE                                  COMMENT
      ASH701945G_M    SCIS                                  COMMENT
      ASH701946.3     NONE                                  COMMENT
      TRM29659.00     UNAV                                  COMMENT
      TRM41249.00     SCIT                                  COMMENT
      TRM57971.00     TZGD                                  COMMENT
  - additional COPIED calibrations:                         COMMENT
      ASH700936F_C    SNOW                                  COMMENT
      ASH701945B_M    SCIS                                  COMMENT
  - updated ROBOT calibrations:                             COMMENT
      AOAD/M_T        NONE                                  COMMENT
      ASH700936C_M    SNOW                                  COMMENT
      ASH700936E      NONE                                  COMMENT
      ASH701945C_M    NONE                                  COMMENT
      JAVRINGANT_DM   NONE                                  COMMENT
      LEIAR10         NONE                                  COMMENT
      LEIAR20         LEIM                                  COMMENT
      LEIAR25         LEIT                                  COMMENT
      LEIAR25.R3      NONE                                  COMMENT
      LEIAR25.R3      LEIT                                  COMMENT
      LEIAR25.R4      NONE                                  COMMENT
      LEIAR25.R4      LEIT                                  COMMENT
      NOV702GG        NONE                                  COMMENT
      TPSCR.G5        TPSH                                  COMMENT
      TRM29659.00     NONE                                  COMMENT
      TRM29659.00     SCIS                                  COMMENT
      TRM57971.00     NONE                                  COMMENT
      TRM59800.00     NONE                                  COMMENT
      TRM59800.00     SCIS                                  COMMENT
  - updated COPIED calibrations:                            COMMENT
      AOAD/M_TA_NGS   NONE                                  COMMENT
      ASH700936A_M    NONE                                  COMMENT
      ASH700936E_C    NONE                                  COMMENT
      ASH700936F_C    NONE                                  COMMENT
      ASH701073.3     NONE                                  COMMENT
      ASH701945B_M    NONE                                  COMMENT
      ASH701946.2     NONE                                  COMMENT
      RNG80971.00     NONE                                  COMMENT
      TPSCR4          NONE                                  COMMENT
      TRM59800.80     NONE                                  COMMENT
      TRM59800.80     SCIS                                  COMMENT
  - additional GLONASS calibrations:                        COMMENT
      AOAD/M_T        NONE                                  COMMENT
      ASH700936C_M    SNOW                                  COMMENT
      ASH701945C_M    NONE                                  COMMENT
      TRM29659.00     SCIS                                  COMMENT
  - all NGS relative calibrations (too numerous to list     COMMENT
    here)                                                   COMMENT
                                                            COMMENT
                                                            COMMENT
Special Notes:                                              COMMENT
This file mostly contains antenna calibrations from the     COMMENT
igs14.atx. For some antennas not included in igs14.atx,     COMMENT
this file is supplemented with NGS relative antenna         COMMENT
calibrations which have been converted to absolute.         COMMENT
For more detailed information,                              COMMENT
see http://www.ngs.noaa.gov/ANTCAL/FAQ.xhtml                COMMENT
                                                            COMMENT
Compiled by National Geodetic Survey                        COMMENT
########################################################### COMMENT
                                                            END OF HEADER
                                                            START OF ANTENNA
BLOCK IIA        G01                 G032      1992-079A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1992    11    22     0     0    0.0000000                 VALID FROM
  2008    10    16    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2319.50                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2319.50                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIA        G01                 G037      1993-032A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2008    10    23     0     0    0.0000000                 VALID FROM
  2009     1     6    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2289.30                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2289.30                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIR-M      G01                 G049      2009-014A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2009     3    24     0     0    0.0000000                 VALID FROM
  2011     5     6    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
      0.00      0.00    963.20                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
      0.00      0.00    963.20                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIA        G01                 G035      1993-054A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2011     6     2     0     0    0.0000000                 VALID FROM
  2011     7    12    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2574.20                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2574.20                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIF        G01                 G063      2011-036A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2011     7    16     0     0    0.0000000                 VALID FROM
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    394.00      0.00   1501.80                              NORTH / EAST / UP
   NOAZI    6.10    4.40    2.80    1.30   -0.20   -1.40   -2.80   -3.90   -4.40   -4.40   -3.70   -2.30   -0.20    3.00    5.70   12.40   18.20   23.50
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    394.00      0.00   1501.80                              NORTH / EAST / UP
   NOAZI    6.10    4.40    2.80    1.30   -0.20   -1.40   -2.80   -3.90   -4.40   -4.40   -3.70   -2.30   -0.20    3.00    5.70   12.40   18.20   23.50
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK II         G02                 G013      1989-044A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1989     6    10     0     0    0.0000000                 VALID FROM
  2004     5    12    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2658.40                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2658.40                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIR-B      G02                 G061      2004-045A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2004    11     6     0     0    0.0000000                 VALID FROM
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
      1.30     -1.10    728.80                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
      1.30     -1.10    728.80                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK I          G03                 G011      1985-093A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  14.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1985    10     9     0     0    0.0000000                 VALID FROM
  1994     4    17    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    210.00      0.00   1884.50                              NORTH / EAST / UP
   NOAZI   -1.00   -2.60   -1.20   -0.90    0.50    1.40    2.00    2.00    1.70    0.50   -0.10   -0.60   -0.70   -0.60   -0.30
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    210.00      0.00   1884.50                              NORTH / EAST / UP
   NOAZI   -1.00   -2.60   -1.20   -0.90    0.50    1.40    2.00    2.00    1.70    0.50   -0.10   -0.60   -0.70   -0.60   -0.30
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIA        G03                 G033      1996-019A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1996     3    28     0     0    0.0000000                 VALID FROM
  2014     8    18    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2757.30                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2757.30                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIA        G03                 G035      1993-054A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2014     9     5     0     0    0.0000000                 VALID FROM
  2014    10    20    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2574.20                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2574.20                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIF        G03                 G069      2014-068A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2014    10    29     0     0    0.0000000                 VALID FROM
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    394.00      0.00   1550.60                              NORTH / EAST / UP
   NOAZI    6.10    4.40    2.80    1.30   -0.20   -1.40   -2.80   -3.90   -4.40   -4.40   -3.70   -2.30   -0.20    3.00    5.70   12.40   18.20   23.50
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    394.00      0.00   1550.60                              NORTH / EAST / UP
   NOAZI    6.10    4.40    2.80    1.30   -0.20   -1.40   -2.80   -3.90   -4.40   -4.40   -3.70   -2.30   -0.20    3.00    5.70   12.40   18.20   23.50
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK I          G04                 G001      1978-020A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  14.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1978     2    22     0     0    0.0000000                 VALID FROM
  1985     7    17    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
ATTENTION! ROUNDED BLOCK MEAN Z-OFFSET VALUE!               COMMENT
   G01                                                      START OF FREQUENCY
      0.00      0.00   1900.00                              NORTH / EAST / UP
   NOAZI   -1.00   -2.60   -1.20   -0.90    0.50    1.40    2.00    2.00    1.70    0.50   -0.10   -0.60   -0.70   -0.60   -0.30
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
      0.00      0.00   1900.00                              NORTH / EAST / UP
   NOAZI   -1.00   -2.60   -1.20   -0.90    0.50    1.40    2.00    2.00    1.70    0.50   -0.10   -0.60   -0.70   -0.60   -0.30
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIA        G04                 G034      1993-068A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  1993    10    26     0     0    0.0000000                 VALID FROM
  2015    11     9    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
    279.00      0.00   2352.40                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
    279.00      0.00   2352.40                              NORTH / EAST / UP
   NOAZI   -0.80   -0.90   -0.90   -0.80   -0.40    0.20    0.80    1.30    1.40    1.20    0.70    0.00   -0.40   -0.70   -0.90   -0.90   -0.90   -0.90
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA
                                                            START OF ANTENNA
BLOCK IIR-M      G04                 G049      2009-014A TYPE / SERIAL NO
                                             0    29-JAN-17 METH / BY / # / DATE
     0.0                                                    DAZI
     0.0  17.0   1.0                                        ZEN1 / ZEN2 / DZEN
     2                                                      # OF FREQUENCIES
  2016     2     2     0     0    0.0000000                 VALID FROM
  2016     9    13    23    59   59.9999999                 VALID UNTIL
IGS14_2062                                                  SINEX CODE
   G01                                                      START OF FREQUENCY
      0.00      0.00    963.20                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G01                                                      END OF FREQUENCY
   G02                                                      START OF FREQUENCY
      0.00      0.00    963.20                              NORTH / EAST / UP
   NOAZI   10.70   10.10    8.00    4.60    0.50   -3.80   -7.50   -9.70  -10.30   -9.50   -7.40   -4.10    0.30    6.00   12.10   22.00   30.40   40.60
   G02                                                      END OF FREQUENCY
                                                            END OF ANTENNA