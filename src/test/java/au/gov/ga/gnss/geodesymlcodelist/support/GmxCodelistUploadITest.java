package au.gov.ga.gnss.geodesymlcodelist.support;

import static org.assertj.core.api.Assertions.assertThat;

import au.gov.ga.gnss.geodesymlcodelist.support.aws.AwsLambdaEmptyResponse;
import au.gov.ga.gnss.geodesymlcodelist.support.test.IntegrationTest;

import java.util.List;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import org.testng.annotations.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GmxCodelistUploadITest extends IntegrationTest {

    private static final int MAX_ANTENNA_RADOME_CODES = 963;
    private static final int MAX_RECEIVER_CODES = 450;

    @Test
    public void testCodelistUploadRequestHandler() {
        AwsProxyRequest proxyRequest = new AwsProxyRequest();
        AwsLambdaEmptyResponse response = this.gmxCodelistUploadHandlerLambda.handleRequest(proxyRequest, null);
        assertThat(response).isNotNull();
    }

    @Test(dependsOnMethods = {
        "testCodelistUploadRequestHandler"
    })
    public void checkAntennaRadomeCodelistXmlUploaded() {
        String codeName = "AntennaRadome";
        assertThat(this.checkExistenceOfCodelistXml(codeName)).isTrue();
    }

    @Test(dependsOnMethods = {
        "testCodelistUploadRequestHandler"
    })
    public void checkReceiverCodelistXmlUploaded() {
        String codeName = "Receiver";
        assertThat(this.checkExistenceOfCodelistXml(codeName)).isTrue();
    }

    @Test(dependsOnMethods = {
        "checkAntennaRadomeCodelistXmlUploaded"
    })
    public void checkAntennaRadomeCodelistContent() throws Exception {
        String codeName = "AntennaRadome";
        List<CodeDefinitionPropertyType> codes = this.getCodesFromCodelistXml(codeName);
        assertThat(codes.size()).isGreaterThanOrEqualTo(MAX_ANTENNA_RADOME_CODES);

        CodeDefinitionPropertyType firstCodeType = codes.get(0);
        String firstCodeValue = this.getCodeValue(firstCodeType);
        assertThat(firstCodeValue.length()).isEqualTo(20);
    }

    @Test(dependsOnMethods = {
        "checkReceiverCodelistXmlUploaded"
    })
    public void checkReceiverCodelistContent() throws Exception {
        String codeName = "Receiver";
        List<CodeDefinitionPropertyType> codes = this.getCodesFromCodelistXml(codeName);
        assertThat(codes.size()).isGreaterThanOrEqualTo(MAX_RECEIVER_CODES);

        CodeDefinitionPropertyType firstCodeType = codes.get(0);
        String firstCodeValue = this.getCodeValue(firstCodeType);
        assertThat(firstCodeValue).isNotEmpty();
    }
}
