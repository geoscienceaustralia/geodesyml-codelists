package au.gov.ga.gnss.geodesymlcodelist.support.jaxb;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;

import javax.xml.bind.JAXBElement;

import lombok.extern.slf4j.Slf4j;
import net.opengis.gml.v_3_2_1.CodeType;
import net.opengis.gml.v_3_2_1.CodeWithAuthorityType;
import net.opengis.gml.v_3_2_1.DefinitionType;
import net.opengis.gml.v_3_2_1.StringOrRefType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;
import net.opengis.iso19139.gmx.v_20070417.CodeListDictionaryType;
import net.opengis.iso19139.gmx.v_20070417.ObjectFactory;

import org.testng.annotations.Test;

import au.gov.ga.gnss.geodesymlcodelist.support.jaxb.GmxCodelistMarshaller;

/**
 * Test marshalling and marshalling of ISO19139 {@code gmx:CodeListDictionaryType}.
 */
@Slf4j
public class GmxCodelistMarshallerTest {

    private GmxCodelistMarshaller marshaller = new GmxCodelistMarshaller();

    private static ObjectFactory gmx = new ObjectFactory();

    @Test
    public void marshal() throws Exception {
        CodeListDictionaryType unitsOfMeasure = (CodeListDictionaryType) new CodeListDictionaryType()
            .withId("uom")
            .withDescription(new StringOrRefType().withValue("units of measure"))
            .withName(new CodeType()
                .withValue("uom")
                .withCodeSpace("si")
            );

        unitsOfMeasure.getCodeEntry().addAll(Arrays.asList(
            new CodeDefinitionPropertyType()
                .withCodeDefinition(this.codeDefinition(new CodeDefinitionType()
                    .withId("m")
                    .withDescription(new StringOrRefType().withValue("metre"))
                    .withIdentifier(new CodeWithAuthorityType()
                        .withValue("m")
                        .withCodeSpace("si")
                    )
                )),
            new CodeDefinitionPropertyType()
                .withCodeDefinition(this.codeDefinition(new CodeDefinitionType()
                    .withId("s")
                    .withDescription(new StringOrRefType().withValue("second"))
                    .withIdentifier(new CodeWithAuthorityType()
                        .withValue("s")
                        .withCodeSpace("si")
                    )
                ))
        ));

        StringWriter writer = new StringWriter();
        this.marshaller.marshal(unitsOfMeasure, writer);

        log.info(writer.toString());

        assertThat(unitsOfMeasure).isEqualTo(
            this.marshaller.unmarshal(new StringReader(writer.toString()))
        );
    }

    private JAXBElement<CodeDefinitionType> codeDefinition(DefinitionType definition) {
       return gmx.createCodeDefinition((CodeDefinitionType) definition);
    }
}
