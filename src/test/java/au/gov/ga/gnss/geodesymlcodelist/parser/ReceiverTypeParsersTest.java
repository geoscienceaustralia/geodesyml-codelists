package au.gov.ga.gnss.geodesymlcodelist.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

@Slf4j
public class ReceiverTypeParsersTest {
    private static final String TEST_DATA_DIR = "data/";

    private Set<CodeDefinitionType> igsTabCodes;
    private Set<CodeDefinitionType> aiubTextCodes;

    @Test
    public void testReceiverTypeAiubTextParser() throws IOException {
        String filename = "RECEIVER.";
        ReceiverTypeAiubParser textParser = new ReceiverTypeAiubParser();
        try (Reader reader = this.getTestFile(filename)) {
            aiubTextCodes = textParser.parse(reader);
        }
        log.info("Number of Receiver types found from " + filename + ": " + aiubTextCodes.size());
        assertThat(aiubTextCodes.size()).isEqualTo(48);
    }

    @Test
    public void testReceiverTypeIgsTabParser() throws IOException {
        String filename = "rcvr_ant.tab";
        ReceiverTypeTabParser tabParser = new ReceiverTypeTabParser();
        try (Reader reader = this.getTestFile(filename)) {
            igsTabCodes = tabParser.parse(reader);
        }
        log.info("Number of Receiver types found from " + filename + ": " + igsTabCodes.size());
        assertThat(igsTabCodes.size()).isEqualTo(68);
    }

    @Test(dependsOnMethods = {
        "testReceiverTypeAiubTextParser",
        "testReceiverTypeIgsTabParser"
    })
    private void testAllReceiverTypeParsers() {
        int maxCount = igsTabCodes.size() + aiubTextCodes.size();
        int minCount = Math.max(igsTabCodes.size(), aiubTextCodes.size());
        igsTabCodes.addAll(aiubTextCodes);
        log.info("Total number of receiver types after merge: " + igsTabCodes.size());
        assertThat(igsTabCodes.size()).isBetween(minCount, maxCount);
    }

    private Reader getTestFile(String filename) {
        String filePath = this.TEST_DATA_DIR + filename;
        return new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath));
    }
}
