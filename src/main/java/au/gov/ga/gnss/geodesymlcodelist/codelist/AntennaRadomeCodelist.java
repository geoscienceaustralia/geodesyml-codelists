package au.gov.ga.gnss.geodesymlcodelist.codelist;

import au.gov.ga.gnss.geodesymlcodelist.parser.AntennaRadomeTypeAtxParser;
import au.gov.ga.gnss.geodesymlcodelist.parser.AntennaRadomeTypeI20Parser;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

public class AntennaRadomeCodelist implements Codelist {

    private static final String igs14AtxUrl = "https://files.igs.org/pub/station/general/igs14.atx";
    private static final String ngs14AtxUrl = "https://www.ngs.noaa.gov/ANTCAL/LoadFile?file=ngs14.atx";
    private static final String pcvI20Url = "http://ftp.aiub.unibe.ch/BSWUSER52/GEN/PCV_COD.I20";

    public String getCodeName() {
        return "AntennaRadome";
    }

    public Set<CodeDefinitionType> getAllCodes() throws IOException {
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));

        AntennaRadomeTypeAtxParser atxParser = new AntennaRadomeTypeAtxParser();
        try (Reader reader = this.getUrlReader(this.igs14AtxUrl)) {
            codes.addAll(atxParser.parse(reader));
        }

        try (Reader reader = this.getUrlReader(this.ngs14AtxUrl)) {
            codes.addAll(atxParser.parse(reader));
        }

        AntennaRadomeTypeI20Parser aiubI20Parser = new AntennaRadomeTypeI20Parser();
        try (Reader reader = this.getUrlReader(this.pcvI20Url)) {
            codes.addAll(aiubI20Parser.parse(reader));
        }

        return codes;
    }

    private Reader getUrlReader(String fileUrl) throws IOException {
        URL url = new URL(fileUrl);
        return new InputStreamReader(url.openStream());
    }
}
