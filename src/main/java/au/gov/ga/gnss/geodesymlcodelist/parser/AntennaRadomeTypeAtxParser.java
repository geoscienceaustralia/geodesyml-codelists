package au.gov.ga.gnss.geodesymlcodelist.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

/**
 * Parses ascii ATX files and extracts GNSS antenna-radome types
 */
public class AntennaRadomeTypeAtxParser extends GnssTypeParser {

    private static final String KEYWORD = "TYPE / SERIAL NO";

    @Override
    protected String getCodeTypeName() {
        return "AntennaRadomeType";
    }

    @Override
    public Set<CodeDefinitionType> parse(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.contains(KEYWORD)) {
                CodeDefinitionType code = getCodeDefinitionType(line, true);
                if (code != null) {
                    codes.add(code);
                }
            }
        }

        return codes;
    }
}
