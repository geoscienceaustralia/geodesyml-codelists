package au.gov.ga.gnss.geodesymlcodelist.codelist;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

import java.io.IOException;
import java.util.Set;

public interface Codelist {
    public String getCodeName();

    public Set<CodeDefinitionType> getAllCodes() throws IOException;
}
