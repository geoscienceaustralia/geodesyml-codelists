package au.gov.ga.gnss.geodesymlcodelist.support.gmx;

import java.io.StringWriter;
import java.util.Set;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import au.gov.ga.gnss.geodesymlcodelist.support.jaxb.GmxCodelistMarshaller;
import net.opengis.gml.v_3_2_1.CodeType;
import net.opengis.gml.v_3_2_1.DefinitionType;
import net.opengis.gml.v_3_2_1.StringOrRefType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;
import net.opengis.iso19139.gmx.v_20070417.CodeListDictionaryType;
import net.opengis.iso19139.gmx.v_20070417.ObjectFactory;

public class GmxCodelistCreator {

    private static final String CODE_SPACE = "urn:xml-gov-au:icsm:egeodesy";
    private static ObjectFactory gmx = new ObjectFactory();

    private static GmxCodelistMarshaller marshaller = new GmxCodelistMarshaller();

    public String getGmxCodelistContent(String codeName, Set<CodeDefinitionType> codes) throws JAXBException {
        CodeListDictionaryType dictionary = this.createCodelistDictionary(codeName, codes);
        StringWriter writer = new StringWriter();
        this.marshaller.marshal(dictionary, writer);
        return writer.toString();
    }

    public CodeListDictionaryType createCodelistDictionary(String codeName, Set<CodeDefinitionType> codes) {
        CodeListDictionaryType codelistDictionary = this.getEmptyCodelistDictionary(codeName);
        this.addCodesToCodelistDictionary(codelistDictionary, codes);
        return codelistDictionary;
    }

    private CodeListDictionaryType getEmptyCodelistDictionary(String codeName) {
        String dictionaryDescription = "GeodesyML GNSS " + codeName + " Types";
        String dictionaryId = "GeodesyML_GNSS" + codeName.replaceAll("-", "") + "TypeCode";

        return (CodeListDictionaryType) new CodeListDictionaryType()
            .withId(dictionaryId)
            .withDescription(new StringOrRefType()
                .withValue(dictionaryDescription)
            )
            .withName(new CodeType()
                .withValue(dictionaryId)
                .withCodeSpace(CODE_SPACE)
            );
    }

    private void addCodesToCodelistDictionary(CodeListDictionaryType codelistDictionary, Set<CodeDefinitionType> codes) {
        for (CodeDefinitionType code: codes) {
            codelistDictionary.getCodeEntry().add(
                new CodeDefinitionPropertyType()
                    .withCodeDefinition(this.codeDefinition(code))
            );
        }
    }

    private JAXBElement<CodeDefinitionType> codeDefinition(DefinitionType definition) {
        return gmx.createCodeDefinition((CodeDefinitionType) definition);
    }
}
