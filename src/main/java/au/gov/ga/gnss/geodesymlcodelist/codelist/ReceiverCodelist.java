package au.gov.ga.gnss.geodesymlcodelist.codelist;

import au.gov.ga.gnss.geodesymlcodelist.parser.ReceiverTypeAiubParser;
import au.gov.ga.gnss.geodesymlcodelist.parser.ReceiverTypeTabParser;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

public class ReceiverCodelist implements Codelist {

    private static final String receiverUrl = "http://ftp.aiub.unibe.ch/BSWUSER52/GEN/RECEIVER.";
    private static final String rcvrAntTab = "https://files.igs.org/pub/station/general/rcvr_ant.tab";

    public String getCodeName() {
        return "Receiver";
    }

    public Set<CodeDefinitionType> getAllCodes() throws IOException {
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));

        ReceiverTypeTabParser tabParser = new ReceiverTypeTabParser();
        try (Reader reader = this.getUrlReader(this.rcvrAntTab)) {
            codes.addAll(tabParser.parse(reader));
        }

        ReceiverTypeAiubParser aiubParser = new ReceiverTypeAiubParser();
        try (Reader reader = this.getUrlReader(this.receiverUrl)) {
            codes.addAll(aiubParser.parse(reader));
        }

        return codes;
    }

    private Reader getUrlReader(String fileUrl) throws IOException {
        URL url = new URL(fileUrl);
        return new InputStreamReader(url.openStream());
    }
}
