package au.gov.ga.gnss.geodesymlcodelist.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Set;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

/**
 * Parses plain text files and extracts GNSS receiver types
 */
public class ReceiverTypeAiubParser extends GnssTypeParser {

    private static final String KEYWORD = "RECEIVER TYPE";
    private static final String REMARK = "REMARK:";

    @Override
    protected String getCodeTypeName() {
        return "ReceiverType";
    }

    @Override
    public Set<CodeDefinitionType> parse(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));
        String line;
        boolean startDataBlock = false;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.contains(KEYWORD)) {
                startDataBlock = true;
                bufferedReader.readLine();  // skip the second line with *
                continue;
            } else if (line.contains(REMARK)) {
                startDataBlock = false;
            }

            if (startDataBlock) {
                CodeDefinitionType code = getCodeDefinitionType(line, false);
                if (code != null) {
                    codes.add(code);
                }
            }
        }

        return codes;
    }
}
