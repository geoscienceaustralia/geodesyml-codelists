#!/usr/bin/env bash

set -e

skipTestsOption=${1:-skipSystemTests}

# Build, test and verify all Java components
mvn clean install "-D$skipTestsOption"
