#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"

function usage {
    echo "Usage: $script"
    exit 1
}

if [[ $# -gt 0 ]]; then
    echo "No argument excepted"
    usage
fi

environment="local"
export TF_VAR_region=$AWS_REGION
export TF_VAR_environment=$environment

cd "$(dirname "$(realpath "$0")")"
cd aws/terraform

rm -frv terraform.tfstate.d/$environment/terraform.tfstate
rm -frv .terraform

# The following 4 variables are needed for `tflocal init`. Other local overrides
# are then applied by `tflocal` itself.
export AWS_STS_ENDPOINT=http://localhost:4566
export AWS_S3_ENDPOINT=http://localhost:4566
export S3_HOSTNAME=localhost
export AWS_ACCESS_KEY_ID=test
export AWS_SECRET_ACCESS_KEY=test

#aws --endpoint http://localhost:4566 s3api create-bucket --bucket geodesy-operations-terraform-state-local
if ! aws --endpoint http://localhost:4566 s3 ls s3://geodesy-operations-terraform-state-local &> /dev/null; then
    aws --endpoint http://localhost:4566 s3 mb s3://geodesy-operations-terraform-state-local
fi

tflocal init -backend-config="workspaces/$environment/backend.cfg"
tflocal workspace select $environment || tflocal workspace new $environment
tflocal apply -auto-approve -input=false \
                -var-file="workspaces/$environment/terraform.tfvars" \
                -target aws_s3_bucket.codelists
