resource "aws_s3_bucket" "codelists" {
  bucket = "${local.prefix}-${var.environment}"
  acl    = "public-read"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    expiration {
      days = 90
    }
  }
}

output "geodesyml_codelists_bucket" {
  value = aws_s3_bucket.codelists.bucket
}
