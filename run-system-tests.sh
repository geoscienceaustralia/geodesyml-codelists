#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << EOF
Usage: $script <env>
where
    env is deployment environment, eg., dev, test, or prod
EOF
}

environment=$1

if [ -z "$environment" ]; then
    echo "Error: deployment environment is unspecified"
    printUsage
    exit 1
fi

# Java SDK v1 does not support SSO profiles and web identity token files.
eval "$(aws configure export-credentials --format env)"

# Run system tests only
mvn -Denv="$environment" verify -P system-tests
